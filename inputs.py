inputfolder='./data/' #Where Google sheet TSV data will be stored.
outputfolder='./OUTPUT/' #Where email templates will be saved.
email_announce_template='email_announce_template.html'
email_reminder_template='email_reminder_template.html'
email_invitation_template='email_invitation_template.html'
email_instructions_template='email_instructions_template.html'
email_announce='Announce.html'
email_reminder='Reminder.html'
email_invitation='Invitation.html'
email_instructions='Instructions.html'
abstracts='abstracts.txt'
chairing_tips='TipsForChairing.pdf'
schedule='colloquium.tsv'
#google_sheet_link='https://docs.google.com/spreadsheets/d/19WeUaIOKPAQyMHrx41Qw8r7WUxL9dU0N7KkkaGiE5Fk/edit?usp=sharing'
activiti_link='https://bpm.swin.edu.au/activiti-app/#/login'
CAS_calendar_link='http://astronomy.swin.edu.au/internal/calendars/'
CAS_events_link='http://astronomy.swin.edu.au/internal/events/'
room_booking_link='http://astronomy.swin.edu.au/internal/booking/'
desk_booking_link='http://astronomy.swin.edu.au/internal/deskbook/'
visitors_link='http://astronomy.swin.edu.au/internal/visitors/add.php'

#Structure of the Google spreadsheet:
col_talk_date=0 #Date of colloquium in dd.mm.yyyy.
col_speaker_name=1 #Full name of speaker (string).
col_speaker_title=2 #Title of speaker: 's' for student (i.e. no title will be used), 'd' for doctor (i.e. Dr), 'p' for professor (i.e. Prof.)).
col_speaker_affiliation=3 #Name of institution where the speaker is affiliated (string). If outside of australia, include country name in parenthesis. If more than one, separate with comma.
col_speaker_email=4 #Email of speaker (string).
col_speaker_gendre=5 #Gendre of speaker: 'f' for female, 'm' for male.
col_talk_host=6 #Name of colloquium host (string).

#Other Activiti-related inputs.
pay_entity='11 -Higher Education'
pay_department='2804 -Centre for Astrophysics and Supercomputing'
pay_project='32011 -Astro Tier 1 Centre Funding'
pay_approver2='MMURPHY' #Second approver for travel requests should be Michael Murphy. He should be approver for lunch reimbursement requests.
pay_approver='KGLAZEBROOK' #Karl Glazebrook should be first approver for travel requests.
pay_staff='ETHACKRAY' #Staff approver.
pay_staff_email='ethackray@swin.edu.au' #Email of staff approver.
phone_while_traveling='6654' #Whose is this number???
faculty='18 - Faculty of Science, Engineering and Technology'
