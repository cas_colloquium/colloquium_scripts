import os, sys
import inputs as ip

############

#Description.

#This script will ease CAS colloquium organisation.
#It aims to automatise as many of the tasks as possible, but unfortunately some of the tasks seem to be hard to automatise.

############

#Main task.

while True:
	os.system('clear')
	print 'Chose one of these options:'
	print '    (i) Generate invitation and instructions emails.'
	print '    (s) Add a speaker to the schedule.'
	print '    (t) Instructions on travel requests.'
	print '    (a) Generate announce and reminder emails.'
	print '    (h) Find colloquium host.'
	print '    (l) Instructions on lunch reimbursement.'
	print '    (p) Generate some plots with colloquium statistics.'
	print '    Any other key to exit.'
	deci=raw_input('->')
	#if deci in ['r','R']:
	#	from modules.reload_Google_sheet_data import reload_Google_sheet_data
	#	reload_Google_sheet_data(ip.google_sheet_link, ip.inputfolder)
	#	
	if deci in ['i', 'I']:
		from modules.create_invitation_emails import create_invitation_emails
		create_invitation_emails(ip.inputfolder, ip.outputfolder, ip.email_invitation_template, ip.email_invitation, ip.email_instructions_template, ip.email_instructions)

	elif deci in ['s', 'S']:
		from modules.add_speaker_to_schedule import add_speaker_to_schedule
		add_speaker_to_schedule(ip.inputfolder, ip.CAS_calendar_link, ip.CAS_events_link, ip.room_booking_link, ip.desk_booking_link, ip.visitors_link, ip.schedule)

	elif deci in ['t', 'T']:
		from modules.travel_request import travel_request
		travel_request(ip.activiti_link, ip.phone_while_traveling, ip.faculty, ip.pay_staff, ip.pay_entity, ip.pay_department, ip.pay_project, ip.pay_approver, ip.pay_approver2)

	elif deci in ['a', 'A']:
		from modules.create_announce_emails import create_announce_emails
		create_announce_emails(ip.inputfolder, ip.schedule, ip.outputfolder, ip.email_announce_template, ip.email_announce, ip.email_reminder_template, ip.email_reminder, ip.abstracts)

	elif deci in ['h', 'H']:
		from modules.find_host import find_host
		find_host(ip.inputfolder, ip.chairing_tips)

	elif deci in ['l', 'L']:
		from modules.lunch_reimbursement import lunch_reimbursement
		lunch_reimbursement(ip.activiti_link, ip.pay_entity, ip.pay_department, ip.pay_project, ip.pay_approver2, ip.pay_staff, ip.pay_staff_email)

	elif deci in ['p', 'P']:
		from modules.statistics import statistics
		statistics(ip.inputfolder, ip.schedule, ip.abstracts)

	else:
		print 'Done.'
		break
