import os,sys
import string

print 'It is a bit complicated to get all abstracts and titles easily, since there is not much consistency in the way they are introduced. However speaker names alone seems to work.'
raw_input('Enter to continue (not sure if it will work all the way through...)')

linksfile='./web_colloquium_links.txt'
outputfile='./archival_abstracts.txt'

lfile=open(linksfile,'r')
ofile=open(outputfile,'w')
ofile.close()
ofile=open(outputfile,'a')

speaker=[]
title=[]
abstract=[]
date=[] #Dates seem to be inconsistently written, it may be tricky to get dates properly; I will store them as 01.01.yyyy, with the right year.
year=2000
for link in lfile:
	os.system('wget -O temp.txt %s ' %link)
	tfile=open('temp.txt','r')
	abstract_line=0
	need_abstract=0
	for tline in tfile:
		if abstract_line==1:
			abstract_i.append(tline.replace('<li>','').replace('<ol>','').replace('</ol>','').replace('<div>','').replace('</div>',''))
			if '</div>' in tline:
				abstract_line=0
				abstract_i_done=string.join(abstract_i)
				abstract.append(abstract_i_done)
				need_abstract=0
		#if '"time"' in tline:
		#	date_bad=tline.split('"time">')[1].split('<')[0]
		#	day_f=date_bad.split(' ')[1]
		#	day_str=
		#	month_str=date_bad.split(' ')[0].replace('.','')
		#	year_str=str(year)
		if '"speaker"' in tline:
			if need_abstract==1:
				abstract.append('TBA') #This means that we have a new speaker while the previous did not have abstract. I introduce a TBA abstract.
			speaker.append(tline.split('"speaker">')[1].split('<')[0])
		if '"title"' in tline:
			title.append(tline.split('"title">')[1].split('<')[0])
			date.append('01.01.'+str(year))
			need_abstract=1
		if '"abstract"' in tline:
			abstract_line=1
			abstract_i=[]
	tfile.close()
	year+=1
	if year==2010:
		break #I think from 2010 things changed...

lfile.close()

print 'Check that there are same number of speakers, dates, titles and abstracts:'
print len(speaker)
print len(date)
print len(title)
print len(abstract)

for i in xrange(len(speaker)):
	ofile.write('DATE: %s\n' %date[i])
	ofile.write('SPEAKER: %s\n' %speaker[i])
	ofile.write('TITLE: %s\n' %title[i])
	ofile.write('ABSTRACT: %s\n\n' %abstract[i])

ofile.close()

