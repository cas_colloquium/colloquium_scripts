import os,sys
import string

print 'This script will save name of speakers only, for different years.'
print 'The gendre of the speaker will need to be introduced afterwars manually, I suppose.'

linksfile='./web_colloquium_links.txt'
outputfile='./archival_speakers.txt'

lfile=open(linksfile,'r')
ofile=open(outputfile,'w')
ofile.close()
ofile=open(outputfile,'a')

speaker=[]
year=2000
for link in lfile:
	ofile.write('YEAR: %s \n' %str(year))
	os.system('wget -O temp.txt %s ' %link)
	tfile=open('temp.txt','r')
	for tline in tfile:
		if '"speaker"' in tline:
			speaker=tline.split('"speaker">')[1].split('<')[0]
			ofile.write('SPEAKER: %s\n' %speaker)
		elif "'speaker'" in tline:
			speaker=tline.split("'speaker'>")[1].split('<')[0]
			ofile.write('SPEAKER: %s\n' %speaker)
	ofile.write('\n')
	tfile.close()
	year+=1

lfile.close()
ofile.close()

