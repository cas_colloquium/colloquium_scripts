import os

def find_host(inputfolder, chairing_tips):
	'''Find a CAS member that will chair the colloquium.'''

	print 'It would be convenient to have a list of CAS members, with a list of keywords of their expertise, and find a suitable potential host based on those keywords (to be matched to the abstract of the talk), and based on how often that CAS member has already chair colloquia recently.'
	print 'An automatic email could be generated with the information about the speaker.'
	print 'For the moment this has to be done manually.'
	print
	print '+Choose a CAS member based on the topic of the talk (it is easy if the speaker collaborates with someone in CAS).'
	print
	print '+If necessary, send that person a document on how to properly chair a colloquium:'
	print '    %s ' %os.path.abspath(inputfolder+chairing_tips)
	print 
	print '+Inform the host about when the speaker arrives, where to meet him/her, reminder about not touching the screen, changing lights, timing, and lunch.'
	print
	raw_input('Press enter to continue.')
