import os

def add_speaker_to_schedule(inputfolder, CAS_calendar_link, CAS_events_link, room_booking_link, desk_booking_link, visitors_link, schedule):
	'''Update the schedule in the CAS web and the local schedule with a new colloquium; also book a room for the talk, a desk for the speaker, and add a CAS visitor.'''
	print 'It would be convenient to have this process automated, but it seems to me hard to do.'
	print 'So, for the moment, it has to be done manually, following the following steps:'
	print
	print '+Check that there are no events planned for the time of the colloquium in the CAS Calendar:'
	print '    %s' %CAS_calendar_link
	print
	print '+Book the room where the colloquium will take place:'
	print '    %s ' %room_booking_link
	print
	print '+Add an event to the CAS Events:'
	print '    %s ' %CAS_events_link
	print 
	print '+Book a desk for the speaker, if needed:'
	print '    %s ' %desk_booking_link
	print
	print '+Add a new CAS visitor:'
	print '    %s ' %visitors_link
	print
	#print '+Update the Google sheet:'
	#print '    %s ' %google_sheet_link
	#print
	print '+Update the schedule:'
	print '    %s' %os.path.abspath(inputfolder+schedule)
	print 
	raw_input('Press enter to continue.')
