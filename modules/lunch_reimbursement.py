
def lunch_reimbursement(activiti_link, pay_entity, pay_department, pay_project, pay_approver2, pay_staff, pay_staff_email):
	print '+To request a lunch reimbursement:'
	print
	print '+Scan restaurant receipt(s) and save as a PDF file(s).'
	print '+Log in to Activiti:'
	print '    %s ' %activiti_link
	print '+Click on Tasks->Start->Reimbursement Claim.'
	print '+Enter your (4-digit) CAS telephone number.'
	print '+Description of expenses: '
	print '    Colloquium lunch for speaker XXX and for host XXX.'
	print '+Upload the scanned receipt(s).'
	print '+In Expense Details, click on "+" to add  the expenses for the colloquium SPEAKER.'
	print '    Enter the date of the lunch.'
	print '    Type: Business meal off campus - Client'
	print '    Enter the cost of the lunch, and UNCHECK the option "GST Applicable".'
	print '    Entity: %s' %pay_entity
	print '    Department: %s' %pay_department
	print '    Project: %s' %pay_project
	print '    Natural account and Natural account description should be automatically filled out'
	print '+In Expense Details, click on "+" to add the expenses for the colloquium HOST (assumed a CAS member).'
	print '    Enter the date of the lunch.' 
	print '    Type: Business meal off campus - Staff'
	print '    Enter the cost of the lunch, and:'
	print '        +If "GST" is written on the receipt, then CHECK the option "GST Applicable".'
	print '        +If "GST" is not written on the receipt, then UNCHECK the option "GST Applicable".'
	print '    Entity: %s' %pay_entity
	print '    Department: %s' %pay_department
	print '    Project: %s' %pay_project
	print '    Natural account and Natural account description should be automatically filled out'
	print '+Enter approver: %s' %pay_approver2
	print '+In Requester Comments you can say again:'
	print '    Colloquium lunch for speaker XXX and for host XXX.'
	print '+Save the form WITHOUTH SUBMITTING!'
	print '+Click again on the same task->SHOW DETAILS->Involve someone else and start collaborating, and add: %s' %pay_staff
	print '+Send an email to staff approver:'
	print '    %s' %pay_staff_email
	print 'Once the email is repplied and the claim is fine, go back to the Activiti claim, and click on submit.'
	print
	raw_input('Press enter to continue.')
