import numpy as np
import os, sys
import get_data as gd
import time

def create_announce_emails(inputfolder, schedule, outputfolder, email_announce_template, email_announce, email_reminder_template, email_reminder, abstracts):
	'''It creates announce and reminder emails, to be sent the week of the colloquium. It identifies the next speaker by date.'''

	print 'It would be convenient if the output were .eml files, that could be opened directly by a mail program, to simply click on "send". The problem is that I do not know how to open .eml files as drafts; they are usually opened as received or sent emails.'
	print 'So, for the moment, the outputs are .html emails, that can be opened with an Internet browser, and its content can be copied into a new email.'
	print

	#Get data (that was originally downloaded from the Google sheet).

	data=gd.get_data(inputfolder, schedule)

	#Get useful items from the data.

	data_items=gd.get_items(data) #Class colloquium contains all useful items.

	#Find next speaker in the schedule.
	date_jd=np.array([gd.date_to_jd(data_items['TALK_DATE'][data_i]) for data_i in xrange(len(data_items['TALK_DATE']))]) #Talk dates in Julian days.
	date_today=gd.date_to_jd(time.strftime('%d.%m.%Y')) #Current date in Julian days.
	indi=np.where(date_jd==min(date_jd[date_jd>date_today]))[0][0] #Index of next talk.

	#Confirm the selected speaker.
	print 'The announce and reminder emails will be generated for:'
	print '    Speaker: %s' %data_items['SPEAKER_NAME'][indi]
	print '    Date: %s' %data_items['TALK_DATE'][indi]
	print
	deci=raw_input('Press enter to continue. Press a to select another speaker.')
	if deci=='a':
		newdate=raw_input('Enter date of talk in dd.mm.yyyy format: ')
		indi=np.where(data_items['TALK_DATE']==newdate)[0][0]

	#Change affiliation from 'AFFILIATION (COUNTRY)' to 'AFFILIATION, COUNTRY'.
	data_items['SPEAKER_AFFILIATION'][indi]=data_items['SPEAKER_AFFILIATION'][indi].replace(' (',', ').replace(')','')

	#Get title and abstract of the talk.
	title, abstract=gd.get_abstract(inputfolder, abstracts, data_items['TALK_DATE'][indi])

	#Load email announce template and generate output announce email.
	ifile=open(inputfolder+email_announce_template, 'r') #Open email template.
	ofile=open(outputfolder+email_announce,'w') #Clear output email file.
	ofile.close()
	ofile=open(outputfolder+email_announce,'a') #Open again to append text.
	email_items=['SPEAKER_NAME', 'TALK_DATE', 'TALK_DATE_FORMATTED', 'TALK_TIME', 'TALK_LOCATION', 'SPEAKER_TITLE', 'SPEAKER_AFFILIATION', 'COFFEE_TIME', 'COFFEE_LOCATION']
	for line in ifile:
		newline=line
		for item in email_items:
			newline=newline.replace('+%s+' %item, '%s' %data_items['%s' %item][indi])
			#Add title and abstract:
			newline=newline.replace('+TALK_TITLE+',title).replace('+TALK_ABSTRACT+',abstract)
		ofile.write(newline)
	ifile.close()
	ofile.close()
	
	print 'Announce email created: %s' %(os.path.abspath(outputfolder)+'/'+email_announce)
	print
       #Load email reminder template and generate output reminder email.
        ifile=open(inputfolder+email_reminder_template, 'r') #Open email template.
        ofile=open(outputfolder+email_reminder,'w') #Clear output email file.
        ofile.close()
        ofile=open(outputfolder+email_reminder,'a') #Open again to append text.
        email_items=['SPEAKER_NAME', 'TALK_DATE', 'TALK_TIME', 'TALK_LOCATION', 'SPEAKER_TITLE', 'SPEAKER_AFFILIATION', 'TALK_TITLE', 'TALK_ABSTRACT', 'COFFEE_TIME', 'COFFEE_LOCATION']
        for line in ifile:
                newline=line
                for item in email_items:
                        newline=newline.replace('+%s+' %item, '%s' %data_items['%s' %item][indi])
                ofile.write(newline)
        ifile.close()
        ofile.close()

	print 'Reminder email created: %s' %(os.path.abspath(outputfolder)+'/'+email_reminder)
	print
	raw_input('Press enter to continue.')


