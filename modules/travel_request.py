
def travel_request(activiti_link, phone_while_traveling, faculty, pay_staff, pay_entity, pay_department, pay_project, pay_approver, pay_approver2):
	'''Fill out a travel request for a speaker traveling from outside of Melbourne.'''
	print 'To complete an Activiti travel request:'
	print
	print '+Log in to Activiti:'
	print '    %s ' %activiti_link
	print '+Click on Tasks->Start->Travel Request.'
	print '+Enter your (4-digit) CAS telephone number.'
	print '+TRAVELLER IDENTIFICATION'
	print '    Traveller type: Other'
	print "    Traveller's name: XXX"
	print '    Phone while traveling: %s' %phone_while_traveling
	print '    Faculty or Centre: %s' %faculty
	print '    Email while traveling: XXX'
	print '+TRAVEL PURPOSES AND OUTCOMES'
	print '    Purpose of trip: Other'
	print '    Please provide purpose if Other selected: CAS Colloquium'
	print '    ->Leave the rest of boxes empty.'
	print '+TRAVEL DETAILS'
	print '    Travel type: Domestic'
	print '    Number of Personal Leave Days: 0'
	print '    In Destination Cities click on "+" and add a city:'
	print '        City: Melbourne.'
	print '        From: XXX.'
	print '        To: XXX.'
	print '        Purpose: Business'
	print '+BOOKING DETAILS'
	print '    CHECK "Indicate if flight bookings required" if flights are to be booked.'
	print '    Airfare Class: Economy'
	print '    CHECK "Indicate if your travel will be organised by a travel booker"' 
	print '        Nominate staff travel booker: %s' %pay_staff
	print '    In Flight Booking Information click on "+" and add a flight:'
	print '        Flight: QFXXX (flight code)'
	print '        Departing from: XXX'
	print '        Departure Date: XXX'
	print '        Departure Time: XXX'
	print '        Destination: Melbourne'
	print '        Arrival Date: XXX'
	print '        Arrival Time: XXX'
	print '        Checked Baggage Required (ask speaker if this is needed, in general yes)'
	print '    ->Add return flight in the same way.'
	print '    In Accommodation Booking Information click on "+" and add accommodation:'
	print '        Arrival Date: XXX'
	print '        Departure Date: XXX'
	print '        Hotel name: XXX'
	print '        Location: XXX'
	print '    ->Leave the rest of boxes empty.'
	print 'ESTIMATED COST OF TRAVEL AND GENERAL LEDGER ACCOUNT CODES'
	print '    In "Estimated Cost Of Travel - List all costs being funded by the University", click on "+" to add an expense.'
	print '        Expense Category: Accommodation - Domestic'
	print '        Estimated Amt $AUD: XXX'
	print '        Entity: %s ' %pay_entity
	print '        Department: %s ' %pay_department
	print '        Project: %s ' %pay_project
	print '        Natural account and Natural account description should be automatically filled out'
	print '        CLICK on Travel Agent.'
	print '    ->Repeat this for Expense Category Airfare - Domestic, for the incoming flight'
	print '    ->Repeat this for Expense Category Airfare - Domestic, for the return flight'
	print '    Additional Information:'
	print "        Speaker's email address: XXX"
	print '        Please also book XXX nights at the XXX hotel.'
	print 'APPROVAL'
	print '    Approver: %s ' %pay_approver
	print '    Second Approver: %s ' %pay_approver2
	print '    Additional Staff to be Notified: %s ' %pay_staff
	print
	raw_input('Press enter to continue.')

