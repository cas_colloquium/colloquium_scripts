import os
import get_data as gd

def create_invitation_emails(inputfolder, outputfolder, email_invitation_template, email_invitation, email_instructions_template, email_instructions):
	''''''
	############

	POTENTIAL_SPEAKER_NAME=raw_input('Input (first) name of invited speaker: ')
	DATE=raw_input('Input tentative date of talk (dd.mm.yyyy): ')

	#Write date in an English formatted way.
	POTENTIAL_TALK_DATE=gd.date_formatted(DATE)

	#Load email invitation template and generate output invitation email.
	ifile=open(inputfolder+email_invitation_template, 'r') #Open email template.
	ofile=open(outputfolder+email_invitation,'w') #Clear output email file.
	ofile.close()
	ofile=open(outputfolder+email_invitation,'a') #Open again to append text.
	email_items=['POTENTIAL_SPEAKER_NAME', 'POTENTIAL_TALK_DATE']
	data_items=[POTENTIAL_SPEAKER_NAME, POTENTIAL_TALK_DATE]
	for line in ifile:
		newline=line
		for item in xrange(len(email_items)):
			newline=newline.replace('+%s+' %email_items[item], '%s' %data_items[item])
		ofile.write(newline)
	ifile.close()
	ofile.close()
	
	print 'Invitation email created: %s' %(os.path.abspath(outputfolder)+'/'+email_invitation)
	print

        #Load email instructions template and generate output instructions email.
        ifile=open(inputfolder+email_instructions_template, 'r') #Open email template.
        ofile=open(outputfolder+email_instructions,'w') #Clear output email file.
        ofile.close()
        ofile=open(outputfolder+email_instructions,'a') #Open again to append text.
        email_items=['POTENTIAL_SPEAKER_NAME']
        data_items=[POTENTIAL_SPEAKER_NAME]
        for line in ifile:
                newline=line
                for item in xrange(len(email_items)):
                        newline=newline.replace('+%s+' %email_items[item], '%s' %data_items[item])
                ofile.write(newline)
        ifile.close()
        ofile.close()

	print 'Instructions email created: %s' %(os.path.abspath(outputfolder)+'/'+email_instructions)
	print
	raw_input('Press enter to continue.')



