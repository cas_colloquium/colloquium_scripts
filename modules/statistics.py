import numpy as np
import os, sys
import get_data as gd
import time
import pylab as py
import string

def statistics(inputfolder, schedule, abstracts):
	'''Generates plots with colloquium data.'''

	print

	#Get data.

	data=gd.get_data(inputfolder, schedule)
	data_items=gd.get_items(data) #All useful items.

	#Total numbers:
	numspeakers=len(data_items.TALK_DATE)
	days=np.zeros(numspeakers)
	for talki in xrange(numspeakers):
		days[talki]=gd.date_to_jd(data_items.TALK_DATE[talki])
	totaldays=max(days)-min(days)
	totalyears=totaldays*1./365.
	totalweeks=totaldays*1./7.

	print 'Number of colloquia in %i days (%.1f years): %i' %(totaldays, totalyears, numspeakers)
	print
	print 'Number of colloquia per week: %.1f' %(numspeakers*1./totalweeks)
	print

	#Data about gendre balance:
	gendre=data_items.SPEAKER_GENDRE
	print 'Percentage of male speakers:'
	print len(gendre[gendre=='m'])*100./len(gendre)
	print

	#Data about speaker titles (grad students, doctors, professors).
	#NOT YET SUCH DATA!!

	#Data about nationality:
	nationality=data_items.SPEAKER_NATIONALITY
	nat_uni=np.unique(nationality)
	freq=np.ones(len(nat_uni))
	for nati in xrange(len(nat_uni)):
		num=len(nationality[nationality==nat_uni[nati]])
		freq[nati]=num

	sorti=freq.argsort()[::-1]
	nat_sorted=nat_uni[sorti]
	freq_sorted=freq[sorti]
	for nati in xrange(len(nat_sorted)):
		print 'Speakers from %s : %i' %(nat_sorted[nati], freq_sorted[nati])
	print

	#Data about colloquium hosts:
	host=data_items.TALK_HOST
	host_uni=np.unique(host)
	host_freq=np.ones(len(host_uni))
	for hosti in xrange(len(host_uni)):
		num=len(host[host==host_uni[hosti]])
		host_freq[hosti]=num

	sorti=host_freq.argsort()[::-1]
	host_sorted=host_uni[sorti]
	host_freq_sorted=host_freq[sorti]
	for hosti in xrange(len(host_sorted)):
		if '?' in host_sorted[hosti]:
			continue
		else:
			print '%s hosted %i times.' %(host_sorted[hosti], host_freq_sorted[hosti])
	print

	#I should get rid of potential/future speakers, since they introduce "?" in the previous statistics.

	#Data about title/abstracts:
	#Count most repeated words from titles and abstracts.
	ifile=open(inputfolder+abstracts)
	all_text=[]
	for line in ifile:
		all_text.append(line)
	full_string=string.join(all_text).lower()

	reject=['"', "\\", ',', '.', '-', '(', ')', '\n', ':', 'title', 'abstract', 'speaker', 'date']
	for rejecti in reject:
		full_string=full_string.replace(rejecti, '')
	strvec=np.array(full_string.split(' '))

	struni=np.unique(strvec)

	str_def=[]
	freq_def=[]
	for stri in xrange(len(struni)):
		if struni[stri].isalpha():
			freq=len(np.where(strvec==struni[stri])[0])
			freq_def.append(freq)
			str_def.append(struni[stri])
	sorti=np.array(freq_def).argsort()[::-1]
	freq_s=np.array(freq_def)[sorti]
	str_s=np.array(str_def)[sorti]

	print 'Total number of words: %i' %len(str_s)
	totalwords=len(str_s)
	print

	#print 'Word | times repeated | percentage'
	for si in xrange(1000):
		#print '%s | %i times | %.1f per cent' %(str_s[si], freq_s[si], freq_s[si]*100./totalwords)
		print '%i - Repeated %i times: "%s"' %(int(si)+1, freq_s[si], str_s[si])

	print

	raw_input('Press enter to continue.')
