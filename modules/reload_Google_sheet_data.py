import os

############

#Define some functions.

def reload_Google_sheet_data(google_sheet_link, inputfolder):
	'''Getting data directly from Google sheets seems to be quite tricky. For the moment I will just describe what to do (manually) to get the data.'''
	print
	print 'It would be convenient to have a function that fetches Google data directly.'
	print 'For the moment, this is not properly implemented, so it has to be done manually!'
	print
	print 'Go to the Google sheet document:'
	print
	print '%s' %google_sheet_link
	print
	print 'Click on File -> Download as -> Tab-separated values (.tsv, current sheet).'
	print 
	print 'Save that file in folder:'
	print
	print '%s' %os.path.abspath(inputfolder)
	print
        print 'Make sure there is only one ".tsv" file in that folder!'
	print
	raw_input('Press enter to continue.')


