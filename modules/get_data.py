import numpy as np
import os, sys
import inputs as ip
import datetime
import astropy.time as at
import string

class AttrDict(dict):
	'''Class that converts dictionary keys into class attributes, from "http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python".'''
	def __init__(self, *args, **kwargs):
		super(AttrDict, self).__init__(*args, **kwargs)
		self.__dict__ = self

def date_to_jd(date):
        '''Converts date from string "dd.mm.yyyy" into Julian days. I actually do not care much about the precise definition of day; I just need a way to sort calendar events continuously.'''
        day=int(date.split('.')[0])
        month=int(date.split('.')[1])
        year=int(date.split('.')[2])
        return int(at.Time(datetime.datetime(year, month, day, 0, 0)).jd)

def date_to_weekday(date):
        '''Converts date from string "dd.mm.yyy" into English week days ("Monday", "Tuesday",...).'''
        day=int(date.split('.')[0])
        month=int(date.split('.')[1])
        year=int(date.split('.')[2])
        weekdays=['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        week_indi=datetime.datetime(year, month, day).weekday()
        weekday=weekdays[week_indi]
        return weekday

def date_formatted(date):
        '''Converts date from string "dd.mm.yyyy" into a formatted English sentence ("Thursday the 21st of July").'''
	month_name=np.array(['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'])
        day_end=np.array(['','st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'st'])
	day=int(date.split('.')[0])
	month=int(date.split('.')[1])
        return '%s, the %i%s of %s' %(date_to_weekday(date), day, day_end[day], month_name[month])

def get_data(inputfolder, schedule):
	'''Load colloquium schedule data.'''
	#Load data from spreadsheet.
	data=np.loadtxt(inputfolder+schedule, dtype='str', delimiter='\t')
	return data

def get_items(d):
	'''Extracts useful items from spreadsheet data ('d').'''
	
	talk_date=[]
	talk_time=[]
	talk_location=[]
	talk_title=[]
	talk_abstract=[]
	speaker_name=[]
	speaker_title=[]
	speaker_affiliation=[]
	speaker_email=[]
	speaker_gendre=[]
	talk_host=[]
	coffee_time=[]
	coffee_location=[]
	speaker_nationality=[]
	talk_date_formatted=[]
	
	#Select useful rows.
	for rowi in xrange(len(d)):
		if len(d[rowi,0])>0: #Skip empty rows.
			if d[rowi,0][0].isdigit(): #Skip useless rows.
				talk_date.append(d[rowi,ip.col_talk_date].split(' ')[0])
				speaker_name.append(d[rowi,ip.col_speaker_name])
				speaker_title.append(d[rowi, ip.col_speaker_title])
				#speaker_affiliation.append(d[rowi, ip.col_speaker_affiliation])
				speaker_email.append(d[rowi, ip.col_speaker_email])
				speaker_gendre.append(d[rowi, ip.col_speaker_gendre])
				talk_host.append(d[rowi, ip.col_talk_host])
				#TO BE IMPLEMENTED:
				talk_time.append('11:30')
				talk_location.append('the VR Theatre')
				talk_title.append('Nice title')
				talk_abstract.append('Wonderful abstract!')
				coffee_time.append('11:00')
				coffee_location.append('the staff room')
				talk_date_formatted.append(date_formatted(d[rowi, ip.col_talk_date].split(' ')[0]))

				affil=d[rowi, ip.col_speaker_affiliation]
				speaker_affiliation.append(affil)
				nationality='Australia'
				if len(affil.split('('))>1: #This means that the speaker comes from outside of Australia, since a nationality is written in parentheses.
					nationality=affil.split('(')[1].split(')')[0]
				speaker_nationality.append(nationality)

	dicti={'SPEAKER_NAME':np.array(speaker_name), 'TALK_DATE':np.array(talk_date), 'TALK_TIME':np.array(talk_time), 'TALK_LOCATION':talk_location, 'SPEAKER_TITLE':np.array(speaker_title), 'SPEAKER_AFFILIATION':np.array(speaker_affiliation), 'SPEAKER_EMAIL':np.array(speaker_email), 'SPEAKER_GENDRE':np.array(speaker_gendre), 'TALK_TITLE':talk_title, 'TALK_ABSTRACT':talk_abstract, 'COFFEE_TIME':coffee_time, 'COFFEE_LOCATION':coffee_location, 'SPEAKER_NATIONALITY':np.array(speaker_nationality), 'TALK_HOST':np.array(talk_host), 'TALK_DATE_FORMATTED':np.array(talk_date_formatted)}
	return AttrDict(dicti)

def get_abstract(inputfolder, abstracts, date):
	'''Get abstract for a talk on a given date (given as a string dd.mm.yyyy).'''
	
	abstract=[]
	ifile=open(inputfolder+abstracts,'r')
	count=0
	abstract_lines=0
	for line in ifile:
		count-=1
		if date in line:
			count=4
		if count==3:	
			speaker=line.split('SPEAKER:')[1]
		if count==2:
			title=line.split('TITLE:')[1]
		if count==1:
			abstract_lines=1
		if abstract_lines==1:
			if 'ABSTRACT:' in line:
				abstract.append(line.split('ABSTRACT:')[1])
			else:
				abstract.append(line)
			if 'DATE:' in line:
				abstract=np.delete(abstract,-1)
				abstract_lines=0
	abstract_full=string.join(abstract).replace('\n', '')

	raw_input('Press enter to get title/abstract for speaker %s' %speaker)
	return title, abstract_full


			

	

